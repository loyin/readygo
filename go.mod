module readygo

go 1.14

require (

	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis/v7 v7.2.0
	github.com/joho/godotenv v1.3.0
	github.com/shopspring/decimal v0.0.0-20200227202807-02e2044944cc
	gopkg.in/square/go-jose.v2 v2.4.1

	gitee.com/chunanyong/logger v1.2.2
	gitee.com/chunanyong/zorm v1.2.2
)
